# Gordon Peter Wills
###### <https://gordywills.com>

-------------------
|Personal Information| | | |
| --- | --- | --- | --- |
|**Address:**|24, Rectory Close|**Telephone:**|+44 (0)1869 320088|
| |Wendlebury|**Mobile:**|+44 (0) 7710 416608|
| |BICESTER|**E-Mail:**|gordy.wills@gmail.com|
| |Oxfordshire, OX25 2PG|**Skype:**|gordon.wills|
| |**Links:**|*LinkedIn:*|https://www.linkedin.com/in/gordon-wills-54a34788|
| | |*Bitbucket:*|https://bitbucket.org/gordywills|
| | |*GitHub:*|https://github.com/izanbard|

Or see my full CV at <https://gordywills.com>

