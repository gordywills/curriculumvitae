FROM php:7.4


WORKDIR /
RUN apt update
RUN apt install -y zip vim nodejs ca-certificates fonts-liberation libappindicator3-1 libasound2 libatk-bridge2.0-0 libatk1.0-0 libc6 libcairo2 libcups2 libdbus-1-3 libexpat1 libfontconfig1 libgbm1 libgcc1 libglib2.0-0 libgtk-3-0 libnspr4 libnss3 libpango-1.0-0 libpangocairo-1.0-0 libstdc++6 libx11-6 libx11-xcb1 libxcb1 libxcomposite1 libxcursor1 libxdamage1 libxext6 libxfixes3 libxi6 libxrandr2 libxrender1 libxss1 libxtst6 lsb-release wget xdg-utils libxrender-dev libfontconfig-dev wkhtmltopdf nginx

RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
#RUN php -r "if (hash_file('sha384', 'composer-setup.php') === '55ce33d7678c5a611085589f1f3ddf8b3c52d662cd01d4ba75c0ee0459970c2200a51f492d557530c71c15d8dba01eae') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
RUN php composer-setup.php
RUN rm composer-setup.php

COPY . /site/
WORKDIR /site
RUN /composer.phar install

RUN mv vendor/h4cc/wkhtmltopdf-amd64/bin/wkhtmltopdf-amd64 vendor/h4cc/wkhtmltopdf-amd64/bin/wkhtmltopdf-amd64.bak
RUN ln -s /usr/bin/wkhtmltopdf vendor/h4cc/wkhtmltopdf-amd64/bin/wkhtmltopdf-amd64

RUN vendor/bin/couscous generate --target ./public ./docs
RUN php bin/generate.php

COPY nginx.conf /etc/nginx/sites-available/default

EXPOSE 5000/tcp
CMD ["/usr/sbin/nginx", "-g", "daemon off;"]
