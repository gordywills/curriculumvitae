<?php
require_once __DIR__ . "/../vendor/autoload.php";

$processed = false;
if (true or $_REQUEST['word'] && $_REQUEST['word'] === 'Gordy') {
    exec('rm -rf '.__DIR__.'/../public/');
    exec(__DIR__ . '/../vendor/bin/couscous generate --target="'. __DIR__ . '/../public" '.__DIR__.'/../docs');
    $input = new Spiritix\HtmlToPdf\Input\StringInput();
    $input->setHtml(file_get_contents(__DIR__.'/../public/index.html'));
    $converter = new Spiritix\HtmlToPdf\Converter($input, new Spiritix\HtmlToPdf\Output\FileOutput());
    $converter->setOption('print-media-type');
    $converter->setOption('d', '300');
    $converter->setOption('margin-bottom', '10mm');
    $converter->setOption('margin-top', '10mm');
    /** @var Spiritix\HtmlToPdf\Output\FileOutput $output */
    $output = $converter->convert();
    $output->store(__DIR__.'/../public/gordywills.pdf');
    $processed = true;
}
$text = $processed ? "success" : "You do not have permission to view this file";
echo <<<HTML
<html>
    <head>
        <title>Generating Document</title>
    </head>
    <body>
        <p>{$text}</p>
    </body>
</html>
HTML;
