apt update
apt install -y zip vim nodejs ca-certificates fonts-liberation libappindicator3-1 libasound2 libatk-bridge2.0-0 libatk1.0-0 libc6 libcairo2 libcups2 libdbus-1-3 libexpat1 libfontconfig1 libgbm1 libgcc1 libglib2.0-0 libgtk-3-0 libnspr4 libnss3 libpango-1.0-0 libpangocairo-1.0-0 libstdc++6 libx11-6 libx11-xcb1 libxcb1 libxcomposite1 libxcursor1 libxdamage1 libxext6 libxfixes3 libxi6 libxrandr2 libxrender1 libxss1 libxtst6 lsb-release wget xdg-utils libxrender-dev libfontconfig-dev wkhtmltopdf
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
php -r "if (hash_file('sha384', 'composer-setup.php') === '55ce33d7678c5a611085589f1f3ddf8b3c52d662cd01d4ba75c0ee0459970c2200a51f492d557530c71c15d8dba01eae') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
php composer-setup.php
rm composer-setup.php
./composer.phar install
mv vendor/h4cc/wkhtmltopdf-amd64/bin/wkhtmltopdf-amd64 vendor/h4cc/wkhtmltopdf-amd64/bin/wkhtmltopdf-amd64.bak
ln -s /usr/bin/wkhtmltopdf vendor/h4cc/wkhtmltopdf-amd64/bin/wkhtmltopdf-amd64
vendor/bin/couscous generate --target ./public ./docs
php bin/generate.php
apt install -y nginx
