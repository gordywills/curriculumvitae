---
currentMenu: home
---
# Gordon Peter Wills
###### https://gordywills.com

-------------------
##Personal Information
|Personal Information| | | |
| --- | --- | --- | --- |
|**Address:**|24, Rectory Close|**Telephone:**|+44 (0)1869 320088|
| |Wendlebury|**Mobile:**|+44 (0)7710 416608|
| |BICESTER|**E-Mail:**|gordy.wills@gmail.com|
| |Oxfordshire, OX25 2PG|**Skype:**|gordon.wills|
| | | | |
|**Links:**| |*LinkedIn:*|https://www.linkedin.com/in/gordywills|
| | |*Bitbucket:*|https://bitbucket.org/gordywills|
| | |*GitHub:*|https://github.com/izanbard|

Getting stuff done that supports a clear and valuable goal is what makes my day. At heart I am an engineer, but leadership is my natural team role; freeing others to empower themselves is very satisfying. It is not just about empowering, but also building a culture that has an action bias; embracing learning through failure and success; prioritising workloads into small iterations that can be easily pivoted (and make failed experiments less impactful whilst still educational); coaching individuals to be their very best; building high performing teams; and about facilitating collaboration between the teams. There are lots of other elements too, but the great thing about it is that the team themselves shape the solutions!

I like to be a hands on engineer, coding solutions in software; in automation of CI and CD (hand in glove with DevOps); or even engineering the processes and tools so that they support not hinder the creative function of an engineering team.  I cut my teeth in small PHP projects before taking on a monolith and refactoring it (over many iterations and a couple of years) to be OO PHP7.1 - much satisfaction.  More recently I have been the team lead looking after a complex web of Python api micro-services and message queue brokers fronted by ReactJS.

I am no stranger to highly dynamic situations having been an army officer for 10 years in both operational and strategic roles. That experience taught me the value of situational leadership and letting the experts do their job. Getting out of the way is often the most important thing a manager can do; that and keeping everyone else out of the way. Then continuing in the fast moving world of Film and TV production for 3 years, which opened new horizons in leadership requiring a more servant style to support the creative process. 

By the time I landed in software engineering my leadership and managerial skills were well honed in a traditional context. To start with that is what I did, but then I met Agile; leadership, teamwork, action bias, and a focus on quality - all built on the ideas of empowerment and engagement.  I was lucky enough to be able to transform the business I was in from pandemonium to well formed Agile.  We pushed an Agile mindset out to the whole business across all functions.   I genuinely enjoy leveraging agile to help teams get stuff done - better.  

------
##Skills

###Leadership
- **Agile**.  From Scrummaster of a single team to leading an engineering department of 5 Scrum teams, I have the experience to manage the processes that back up Agile.  Focusing on quality, I work to empower the front line decision makers to do the right thing in a dynamic situation and ensure we deliver value.  I then work to ensure that these decisions are backed across all business functions.  I have worked with C-grade executives, sales and marketing teams and with professional services teams to help them understand how to be more Agile and how to get the most from working with an Agile engine room.  
- **Coaching**.  Line management can all too easily become a transactional interaction of setting tasks and approving leave; I coach my direct reports and others to facilitate better engagement and empowerment ultimately driving productivity and personal effectiveness.  I use active listening methods to pursue an agenda of autonomy, mastery and purpose for those that I am coaching and seek to balance directive instruction with collaboration.
- **The Servant**.  Servant leadership is fundamental to an Agile mindset and to great coaching.  My wide range of experiences in the military, TV, software development and charity volunteering have given me the confidence to trust front line decision makers and the understanding to be able to facilitate the conditions for them to succeed.  These behaviours are key to an engaged workforce and a sustainable productive environment.
- **Project Management**.  From military procurement through documentary film making to software delivery, I have project management experience in many different contexts.  I am familiar with PRINCE2, Gantt Chart, Balanced Scorecard, ITIL, Critical Path, and latterly Scrum and Kanban methodologies for managing projects.  


###Technical
|skill|description|score
|---|---|---|
|**PHP**|I can code in PHP5 or 7 with strong OO skills.  I have experience of Zend1/2, symfony console, phpunit, RESTful API handling and have used composer extensively.  I have maintained and enhanced a complex SaaS application based in PHP and engaged in a program of significant refactor to move it from a procedural to OO architecture.|**7/10**|
| | | |
|**Python**|I can code python APIs in 3.x with strong OO skills and a reasonable knowledge of the right libraries to use.  I supported the back end of a complex healthcare app composed of microservices and rabbit queues.|**6/10**|
| | | |
|**node.js**|Limited to a hobbyist level of skill, I have written some extensions to a node.js application.  I understand the basics and my extensions have worked well.|**2/10**|
| | | |
|**MySql/ PostgreSQL**|I have written and optimised queries with complex joins and indexes.  I have some ability to review server log files to identify bottlenecks and further enhancements.|**6/10**|
| | | |
|**Neo4J**|I have some limited exposure to Neo4J Graph database and understand the concepts well, but my Cypher is weak.|**4/10**|
| | | |
|**JavaScript**|I have both native and JQuery experience implementing AJAX and manipulating the DOM.  I have used firebug/chrome dev tools to help debug JS.|**6/10**|
| | | |
|**Redis**|I have used Redis NoSQL to replace native PHP session handling, including modelling and setting up of the service.  Having embedded PRedis I then extended its use beyond sessions for translations.|**6/10**|
| | | |
|**Apache/ Nginx**|I have basic Apache2 and Nginx skills that allowed me set up a server and run multiple hosts.  I am conversant enough with the concepts that I can usually find the answer in the Google Manual.|**5/10**|
| | | |
|**Linux (Centos/ Ubuntu)**|I can set up an OS and install a LAMP stack.  I have basic disk configuration skills including logical volumes.  I can do user management, including limiting access to sudo services.  I can set up SSH and SFTP.  In fact using the Google manual there is little I could not take on.|**7/10**|
| | | |
|**Git/ Github/ Bitbucket**|I use DVCS across my personal and professional life.  I fully understand the block nature of git and can use all of its tools.  Although I am still a little inexperienced with rebase.|**7/10**|
| | | |
|**Jira**|I have been a Jira administrator for an instance of 250+ users (both server and cloud).  I can manage and use all aspects of this application, including integration with other Atlassian products.|**8/10**|
| | | |
|**Jenkins**|I have used Jenkins as a build coordinator for both CI and deployment of PHP applications.  I coded separate jobs to run unit tests, static code analysis and then bundled and deployed the code to our staging CI server.|**6/10**|
| | | |
|**CircleCI**|I can set up and run circleci for automated tests, automated docker builds and to a limited extent automated deployments.|**5/10**|
| | | | 
|**New Relic**|I have used New Relic to monitor and alert for a PHP application with over 250k users and have added it to a python microservice architecture.|**5/10**|

-------
##Employment
###Software Team Architect at Intelligent Growth Solutions
**July 2019 - Present**

As climates change and populations increase, the food supply chain is at risk. Current methods for high yield crops are unsustainable and ultimately contribute to global climate change. Enter total controlled environment farming - its an indoor farming method which can increase yield per acre and overall quality while driving down use of harmful chemicals and focusing on energy efficiency. Intelligent Growth Solutions are a start up at the cutting edge of the technology, automating many of the processes, using low voltage systems to make them more energy efficient; backed by the science of the James Hutton Institute so that only the most beneficial light frequencies are used in growing.

My role is to build a world class software team that can produce a management software to couple with the cutting edge hardware to form a truly exceptional product. Getting back together with some of the Northface Ventures team, we are building a college of experts to develop and realise the product - we will be focusing on getting the culture right so that we can satisfy a hungry market that wants to feed the world.

###Head of Software Engineering at Sensyne Health
**July 2018 - July 2019**

Sensyne Health is a health tech business that provides Digital health apps to the NHS in support of specific clinical conditions.  Whilst helping individuals get better health care, we are ethically sourcing data across a patient population which we then analyse to improve patient population care and support new drug discovery in the pharmaceutical industry.

My role as Head of Software Engineering is to manage a team of 20+ engineers, testers, DevOps engineers and product owners in delivery of the digital health applications.  As well as owning the architecture and development process, I code solutions and am heavily involved in the tooling, integration and deployment mechanisms for the team.  I also work with my data engineering and systems medicine peers to ensure a smooth flow of data and requirements throughout the business model.

###Chief Instructor at Team Rubicon UK
**October 2017 - March 2018 (6 months)**

[Team Rubicon UK](index.html#volunteer-work) sends people to disaster zones shortly after the event in order to help people survive and re-establish normal routines.  In the aftermath of Huricanes Irma and Maria, Team Rubicon UK sent over 100 volunteers to the caribbean islands to help.  My role was to plan and conduct both pre and post deployment training packages to ensure volunteers were fully prepared.  I was also responsible for updating and delivering an induction training package to the 100s of new volunteers that joined after these disasters.

###Agile Coach/Engineer at Northface Ventures
**September 2016 - July 2018 (1 year 10 months)**

The Northface Ventures team were part of the crew who built and sold Workplace Systems after a three year journey of transformation from stale software house to vibrant product-driven company. Now Northface share what we learned.

My role was the engine room consultant. Working with engineering departments to build high performing teams delivering excellent product that satisfies market requirement. I assessed the engineering departments that I worked with to determine where to invest effort for improvement and where to cut out wasteful practice. I delivered coaching for engineers and engineering managers on agile concepts and implementation methodologies including Scrum and Kanban.  Helping to implement modern engineering practices streamlining end to end delivery and foster a corporate culture of working together to deliver product.  

###Product Development Manager at Workplace Systems
**April 2014 - August 2016 (2 years 4 months)**

I led a team of 30+ software and QA engineers to deliver the flagship enterprise level application from Workplace Systems. This role coincided with a period of rapid growth after having repositioned ourselves in the market.  Having started to implement Scrum in my previous role in the company, this role required that we scale that scrum effort, ultimately creating 5 scrum teams.  Further, with a focus on quality, I also implemented a Scrumban team solely focused on customer tickets and performance.  I also built on the kernel of a DevOps team to integrate infrastructure and engineering much more closely and adding more and more automation into build and deployment.  Architecturally we moved the product to PHP5.6 (no easy feat) backed with a MySql move from 5.0 to 5.6.  This allowed the scrum teams to set higher standards for code review and accelerated the refactor programme.

During this time I also worked with wider business to help the professional services department to convert to using agile project management.  I also spent 2 months in Sydney working with our Australian office, primarily to integrate an engineering team based there in to our global team, but also working with business management, sales and services teams to foster our core culture and embrace Agile thinking. In the later months of this role, as the company was being sold to a US firm, I spent several weeks in the US sharing best practice and standards from the UK engineering team with a larger US team - ensuring common procedures and methodologies and helping them develop their nascent adoption of Scrum. 

###Software Delivery Manager at Workplace Systems
**March 2013 - April 2014 (1 year 1 month)**

I was the scrummaster for 2 scrum teams while we worked out how to do scrum.  Due to a change of senior management, this was a time of turnaround and transformation at Workplace Systems; we moved from being a services company with a software offering to being product led.  With that high level of change across all business functions, talent management was of the first importance - keeping the talent we had and attracting fresh thinkers.  In addition to delivering user stories and being the sole deployment engineer, I set up a recruitment programme and trained new managers to share the load.  I also started converting the deployment process from a largely manual process to a scripted process that ultimately went on to be a Jenkins job - this was the start of DevOps in the company.  As a software delivery manager, my main focus for the product was to get us in to a position to upgrade to PHP5.3 (that's not a typo) and to bring an experimental zend based mobile offering to fruition.

The change of senior management was far from seamless, in fact all the C-Grades were let go in a single hit.  Whilst the board chair stepped into the position of CEO, we were with out a CTO for 3 months.  Being the senior tech leader, it fell to me to work with the new CEO to reassure our client base and our own employees; keeping the ship moving.  I assisted with the appointment of our new CTO and helped with his initial integration with the team and business.

###PHP Software Engineer at Workplace Systems
**September 2011 - March 2013 (1 year 6 months)**

As a PHP engineer my role was to enhance and maintain a procedural code base with a mix of PHP4 and PHP5 standards and over 500k lines in over 100k files.  In addition to squashing bugs and delivering features, I spent a lot of time refactoring code to simplify it and make it ready for further refactor to OO at a later date. I also started to gather knowledge of our deployment processes from several remote workers (including in Australia) and consolidating our version control and ticketing systems.  

###Assistant Producer/Location Manager for Film and Television
**March 2008 - October 2011 (3 years 7 months)**

I worked mostly in documentary making and comedy shows.  The short contract nature of the work, meant building teams very quickly to be able to deliver high quality from day 1.  I worked for many production companies including Tiger Aspect and TwoFour.  These roles were a great shake out after the military and allowed me to see the world from many different perspectives.

###Self-Employed Web Developer
**March 2008 - September 2011 (3 years 6 months)**

Taking small contracts between working in the TV industry I built simple browser apps from scratch and enhanced/fixed complex and well established brand websites.  This included work for Oxford Outcomes on academically rigorous survey applications and website work for smartlogic.com, shirtworks.co.uk and anchorvans.co.uk.

###Officer in the British Army
**September 1998 - March 2008 (9 years 6 months)**

Royal Signals Officers are the army's technical leaders, both commanding highly capable technical soldiers and shaping technology policy and procurement.  Throughout my career as a Royal Signals Officer I completed duties in both sides of the role.

####Command
- *Troop Command*. Command of 30+ soldiers to provide tent based mobile office for a general and his staff including all radio, telephone and computing services.
- *Exchange to Royal NZ Signals*. Part of an annual exchange to share leadership and cultural information.
- *Recruit Training*. Command of a training team of 5 soldiers taking 45 16yr olds from new recruit to trade qualified.  
- *2IC 19th Light Brigade Signal Squadron*. Second in Command of a unit of 212 soldiers providing mobile offices; particularly responsible for discipline, personnel, ceremonial and administrative matters.

####Technical
- *Technical Adjutant 16th Signal Regiment*.  I wrote and verified technical requirements for procurement of all regimental specialist communications equipment.  When deployed I  managed a global reach back communication network connecting UK based military and governmental command with deployed formations.
- *Staff Officer Grade 3 Communications Multi-National Brigade(North East)*.  A deployed role in Bosnia, I procured urgent operational requirements for all units in the brigade and specialist units in our part of the theatre.  I was also responsible for ensuring that the technical aspects of the Dayton Accord were being complied with.
- *Staff Officer Grade 3 Intelligence, Surveillance, Target Acquisition and Reconnaissance (Electronic Warfare) at the Command Support Development Centre (Networks)*.  As part of the Army's competent authority for electronic warfare, I procured new electronic warfare equipment both off the shelf and as part of the equipment programme, along with other duties.

------
##Education
###Royal Military Academy Sandhurst
*1998 - 1999*

Commissioned into Royal Signals

**Trained in:** Leadership, Communication Skills, Military Analysis (History), Soldiering Skills

###University of Warwick
*1995 - 1998*

2:2 Bachelor of Science (BSc), Computer Science

**Activities and Societies:** Warwick Aikido Club, FRIMAG technical group, SU Stage crew

###Duke of York's Royal Military School (DYRMS)
*1988 - 1995*
* 4 A levels (Maths(B), Chemistry(B), Physics(B), Biology(D))
* 1 AS Level (General Studies(D))
* 11 GCSE (including English and Maths)

------
##Volunteer Work
###Team Rubicon (https://www.teamrubiconuk.org/)
Team Rubicon unites the skills and experiences of military veterans with first responders to rapidly deploy emergency response teams in the UK and around the world. As a greyshirt member of the team I am a volunteer responder able to bring my full range of skills and experience to help people in the aftermath of a disaster.  

###Wendlebury Village Website 
I help to keep a WordPress site secure and up to date and train others in adding content.  We also use Mailchimp as a newsletter to advertise events and local news to the village and again I train users and administer our account.  We also have Facebook, Twitter and Google presence.  

###Wendlebury Village Committee
Wendlebury village is an awesome place to live with a great community and friendly atmosphere.  The village committee organises and runs regular social events including an annual comedy night, a traveling supper, summer BBQ and quiz nights.  My roles are many and various throughout the planning and execution of the events and include cocktail barman, MC, flyer delivery and technical "stage crew" support. 

------
##Selection of Open Source Repos
###<https://github.com/izanbard>
This is a collection of extensions for the [Magic Mirror<sup>2</sup>](https://github.com/MichMich/MagicMirror) framework.  Some of them are forked from an original extension where I have created bug fixes or extensions to the behaviour - in each case I have had my work accepted and merged back in to the prime repo.  

###<https://bitbucket.org/gordywills/clock>
This is a super simple Javascript clock.  I have it running on a Raspberry Pi with a 7 inch monitor on a boot up script that runs it in a headless chromium browser.

###<https://bitbucket.org/gordywills/smart-lcd>
This is a composer installable PHP library to control a hardware interface from a Raspberry Pi GPIO to a HD44780 Smart LCD driver.  I developed library as when I came to use an LCD display in one of my hobbyist projects there were no PHP libraries available.

###<https://bitbucket.org/gordywills/server-ip>
This is a composer installable utility to get the various IP addresses of the machine it is running on.  This is useful in the context of either a CLI script of some sort of DevOps tool. I developed this library and a single interface to provide the utility from a number of methods of getting IP Addresses.

------
##References

**Professional**: David Farquhar (Manager at Northface Ventures and Workplace Systems)

<david.farquhar@northfaceventures.com>

Address on request

**Personal**: Rien Sach (Colleague and Friend at Workplace Systems)

<rien.j.sach@gmail.com>

Address on request

------
##Interests and Hobbies
- **Gaming**.  Online you will find me playing MMOs like Planetside2 or Elite:Dangerous - my Steam/gamer tag is *Izanbard*.  Offline I like to play board games and card games including Pandemic and Carcassonne.  As a combination of the two I love a good game of Keep Talking.
- **Astronomy**.  Having started as a naked eye observer during my travels with the military, I now have a 200mm reflector ideal for looking at the planets.  I have visited Griffiths, Sydney and Kielder observatories and plan to visit Hawaii, Atacama and Parkes some time in the future.  In the mean time, when the weather is good, I like to spark up a fire pit and watch the majesty of the heavens from right here in Oxfordshire.
- **Film**.  I am a fan of big blockbusters and the comic book franchises, but I also like films that show you a new perspective or tell a real human story.  Action is entertaining escapism, but storytelling helps you to walk in someone else's shoes for a while.

------